// This is a translation of https://static.gnome.org/friends/ruler/ruler-fog.js

document.write( '<style type="text/css">' + 
		'#donation-banner { background: #4cae4c; color: #fff; padding: 0 10px; font-size: 12px; }' +
		'#donation-banner a { color: #fff; }' +
		'#donation-banner p { display: inline-block; }' +
		'#donation-banner img { margin-right: 5px; width: 24px; height: 24px; vertical-align: middle; }' +
		'#donation-banner .donation-ruler { background: white; width: 210px; display: inline-block; padding: 2px; margin: 0 10px; }' +
		'#donation-banner .ninesix { margin: auto; width: 100%; text-align: center;}' +
		'#donation-banner .donation-button { border: 1px solid #555753; background-image: linear-gradient(to bottom, #73d216 0%, #4cae4c 100%); padding: 5px 10px; border-radius: 5px; text-decoration: none; color: #fff !important; font-weight: bold; }' +
		'</style>' )

document.write( '<div id="donation-banner">' +
			'<div class="ninesix">' +
			'<img src="https://static.gnome.org/friends/ruler/lock.png">' +
			'<p>Doe agora para ajudar o GNOME a <a href="https://www.gnome.org/support-gnome/" style="margin-right: 20px;">continuar nosso trabalho!</a></p>' +
			'<a href="http://www.gnome.org/support-gnome/" class="donation-button">Doar agora!</a>' +
			'</div>' +
		'</div>' +

		'<div style="clear: both;"></div>')
